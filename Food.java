/**
 * A class the represents food for our party.
 */

public class Food
{
    private String description;
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    
    public void setDescription(String inDescription)
    {
        this.description = inDescription;
    }
    
}